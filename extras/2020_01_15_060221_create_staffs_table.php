<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('department');
            $table->string('job_title');
            $table->float('salary');
            $table->string('sss');
            $table->string('tin');
            $table->string('philhealth');
            $table->datetime('date_hired');
            $table->timestamps();


            $table->unsignedBigInteger('user_roleid')->nullable()->default(3);
            $table->foreign('user_roleid')
                ->references('id')->on('roles')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
