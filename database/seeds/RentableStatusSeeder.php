<?php

use Illuminate\Database\Seeder;
use App\Rentable_Status;
use Carbon\Carbon;

class RentableStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        Rentable_Status::insert([
          [

            'name' => 'available',
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()  

          ],

          // all statuses below are considered unavailable for rent

          [
            'name' => 'rented',
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString() 
          ],
          [

            'name' => 'damange',
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString() 
          ],
          [

            'name' => 'maintenance',
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString() 
          ]



        ]);

    }
}
