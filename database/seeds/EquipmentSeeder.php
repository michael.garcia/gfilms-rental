<?php

use Illuminate\Database\Seeder;
use App\Equipment;
use Carbon\Carbon;

class EquipmentSeeder extends Seeder
{
  /**
      * Run the database seeds.
      *
      * @return void
      */
     public function run()
     {
         

        Equipment::insert([

          [
            'name' => 'Cameras',
            'current_available' => 2,
            'current_out' => 1,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ],
          [
            'name' => 'Lenses',
            'current_available' => 0,
            'current_out' => 0,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ],
          [
            'name' => 'Lights',
            'current_available' => 0,
            'current_out' => 0,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ],
          [
            'name' => 'Audio',
            'current_available' => 0,
            'current_out' => 0,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ],
          [
            'name' => 'Stabilizers',
            'current_available' => 0,
            'current_out' => 0,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ],
          [
            'name' => 'Others',
            'current_available' => 0,
            'current_out' => 0,
            'created_at' => Carbon::now()->toDateTimeString(), 
            'updated_at' => Carbon::now()->toDateTimeString()
          ]



        ]);

     }
}
