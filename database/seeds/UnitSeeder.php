<?php

use Illuminate\Database\Seeder;
use App\Unit;
use Carbon\Carbon;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 

        Unit::insert([
            [
                'name' => 'Canon C300',
                'manufacturer'=> 'Canon',
                'year_model'=> '2012',
                'description'=> 'The Canon EOS C300 Cinema Camera is made for amazing cinematography. Made by Canon, a brand trusted by both professional and casual photographers worldwide, you can be very well-assured of its quality. Videos done with this camera pack a mighty graphic punch that leaves everyone impressed. It is powered by Super 35 24.6 x 13.8mm effective size (6.4 x 6.4µm pixel pitch), backed by CMOS sensor to capture rich color and line details with great accuracy. It also uses Dual Pixel CMOS for its autofocus system to make for maximum blur-reduction. It has an ISO sensitivity range of 320 to 80,000 to take clear shots even under low key lighting conditions.',
                'rent_price' => 10000,
                'equipment_id' => 1,
                'rentable_status_id' => 1,
                'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString()
            ], 
            [
                'name' => 'Sony FS700',
                'manufacturer'=> 'Sony',
                'year_model'=> '2013',
                'description'=> 'The NEX-FS700P is an NXCAM camcorder with a superior Super35mm CMOS sensor, super slow-motion capability and an interchangeable E-mount lens system, offering unrivalled flexibility and creative expression.',
                'rent_price' => 10000,
                'equipment_id' => 1,
                'rentable_status_id' => 1,
                'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => 'Sony FS5',
                'manufacturer'=> 'Sony',
                'year_model'=> '2015',
                'description'=> '4K Super 35 Exmor sensor with 11.6 million total pixels and 8.8 million effective pixels,
                4K 30p recording in XAVC Long GOP codec and 4K 60p/50p in FS RAW format with optional CBKZ-FS5RIF upgrade kit,
                Built-in electronic Variable ND Filters with linear 1/4 to 1/128 and auto ND,
                240fps HFR in Full HD and 240fps HFR in 4K with FS RAW output with optional CBKZ-FS5RIF upgrade kit,
                E-mount flexibility',
                'rent_price' => 12000,
                'equipment_id' => 1,
                'rentable_status_id' => 2,
                'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }
}
