<?php

use Illuminate\Database\Seeder;
use App\Request_Status;
use Carbon\Carbon;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Request_Status::insert([
             		[
             			'name' => 'approved',
             			'created_at' => Carbon::now()->toDateTimeString(), 
                        'updated_at' => Carbon::now()->toDateTimeString()
             		],
             		[
             			'name' => 'denied',
             			'created_at' => Carbon::now()->toDateTimeString(), 
             			'updated_at' => Carbon::now()->toDateTimeString()
             		],
             		[
             			'name' => 'pending',
             			'created_at' => Carbon::now()->toDateTimeString(), 
             			'updated_at' => Carbon::now()->toDateTimeString()
             		],
             		[
             			'name' => 'completed',
             			'created_at' => Carbon::now()->toDateTimeString(), 
             			'updated_at' => Carbon::now()->toDateTimeString()
             		]
             	]);

    }
}
