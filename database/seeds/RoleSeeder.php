<?php

use Illuminate\Database\Seeder;
use App\Role;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
  /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	Role::insert([
     		[
     			'name' => 'admin',
     			'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString()
     		],
     		[
     			'name' => 'member',
     			'created_at' => Carbon::now()->toDateTimeString(), 
     			'updated_at' => Carbon::now()->toDateTimeString()
     		],
     		[
     			'name' => 'staff',
     			'created_at' => Carbon::now()->toDateTimeString(), 
     			'updated_at' => Carbon::now()->toDateTimeString()
     		]

     	]); 
    }
}
