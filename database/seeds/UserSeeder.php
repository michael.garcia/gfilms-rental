<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
         User::insert([
            [
                'name' => 'Mikez',
                'email'=> 'michaelgarcia101@gmail.com',
                'password'=>Hash::make('superadmin'),
                'role_id'=>2,
                'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString()

            ],
            [
                'name' => 'Lizvel',
                'email'=> 'mikez2@gmail.com',
                'password'=>Hash::make('superadmin'),
                'role_id'=>2,
                'created_at' => Carbon::now()->toDateTimeString(), 
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [

              'name' => 'Joy',
              'email'=> 'joy@gmail.com',
              'password'=>Hash::make('superadmin'),
              'role_id'=>3,
              'created_at' => Carbon::now()->toDateTimeString(), 
              'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [

              'name' => 'Admin',
              'email'=> 'admin@gmail.com',
              'password'=>Hash::make('superadmin'),
              'role_id'=>1,
              'created_at' => Carbon::now()->toDateTimeString(), 
              'updated_at' => Carbon::now()->toDateTimeString()
            ]


         ]); 

  }
}