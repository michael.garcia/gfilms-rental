<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentrequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentrequests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('request_code')->unique();
            $table->float('rent_total');
            $table->integer('equipment_total');
            $table->timestamps();


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('set null')
                  ->onUpdate('set null');



            $table->unsignedBigInteger('request_status_id')->nullable();
            $table->foreign('request_status_id')
                  ->references('id')->on('request_statuses')
                  ->onDelete('set null')
                  ->onUpdate('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentrequests');
    }
}
