<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('units', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('manufacturer');
                $table->string('year_model');
                $table->string('photo')->nullable();
                $table->mediumText('description');
                $table->float('rent_price');
                $table->timestamps();


                $table->unsignedBigInteger('equipment_id')->nullable();
                $table->foreign('equipment_id')
                    ->references('id')->on('equipments')
                    ->onDelete('set null')
                    ->onUpdate('set null');


                $table->unsignedBigInteger('rentable_status_id')->nullable()->default(1);
                $table->foreign('rentable_status_id')
                    ->references('id')->on('rentable_statuses')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('units');
        }
}
