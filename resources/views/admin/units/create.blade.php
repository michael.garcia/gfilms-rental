{{-- Create -> Store New Unit to Db --}}
{{-- create.blade.php --}}


@extends('layouts.app')
@section('content')


<div class="container">
	<h1 class="text-center">Create New Unit</h1>
	<hr>
	<div class="row">
		@csrf
		@if($errors->hasAny())
				<div class="alert alert-danger mb-0 p-0">
						{{ $errors }}
				</div>
		@endif
		<div class="col col-12 col-md-8 mx-auto">
				{{-- Start of Card --}}
				<div class="card elegant-color white-text rounded-bottom">
					<img class="card-img-top" src="https://via.placeholder.com/708x472" alt="Unit Image">
			    	<div class="card-body">
					<form action="{{ route('units.store') }}" method="POST" enctype="multipart/form-data">
					@csrf
						<input type="file" name="photo" id="photo" class="form-control-file mb-1">
						<hr>
				    	<input type="text" name="name" id="name" class="form-control mb-1" placeholder="Unit Name" value="">

				    	<input type="text" name="manufacturer" id="manufacturer" class="form-control mb-1" placeholder="Manufacturers Name" value="">

				    	<input type="text" name="year_model" id="year_model" class="form-control mb-1" placeholder="Year Model" value="" max="4" min="4">


				    	<select name="equipment_id" id="equipment_id" class="custom-select">
				    				@foreach($equipments as $equipment)
				    				<option 
				    				value="{{ $equipment->id }}"
				    				{{ old('equipment_id') == $equipment->id ? "selected": "" }} 
				    				{{-- simplified if-else --}}

				    				>

				    				{{ $equipment->name }}
				    				@endforeach()
				    	</select>
						
				    	<input type="text" name="rent_price" id="rent_price" class="form-control mb-1 mt-1" placeholder="Rent Price" value="">

				    	<textarea name="description" id="description" cols="30" rows="10" placeholder="Product Description" class="form-control mb-1"></textarea>

						<button type="submit" class="btn btn-dark">Create New Unit</button>
						</form>
			    	</div>
					
				</div>
				{{-- End Card --}}
			
		</div>
	
		<div class="col col-12 col-md-4 mx-auto">
			<a href="{{ route('units.index') }}"><h2 class="bg-primary text-light text-center rounded p-2 ">VIEW UNITS</h2></a>
			<h2 class="bg-dark text-light text-center rounded p-2 ">STATISTICS</h2>
			<div class="table-responsive rounded">
				<table class="table table-hover ">
					<thead>
						<th class="text-center">LEGEND</th>
						<th class="text-center">FIGURE</th>
					</thead>
					<tbody>

						<tr class="#">
							<td><strong>Status</strong></td>
							<td>{Status}</td>
						</tr>
						<tr>
							<td>Last Rented Date</td>
							<td>{Rented Date}</td>
						</tr>
						<tr>
							<td>Last Used By</td>
							<td>{Rented User}</td>
						</tr>
						<tr>
							<td>Total Times Rented</td>
							<td>{20}</td>
						</tr>
						<tr>
							<td>Gross Income</td>
							<td>&#8369; {{ number_format(150000,2)  }}</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection