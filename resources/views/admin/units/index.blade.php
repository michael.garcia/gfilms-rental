{{-- Units -> Index/Show All --}}
{{-- index.blade.php --}}

@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			<h3>LIST OF UNITS </h3>
		</div>
		<div class="col-12 col-md-2 mx-auto">
			<a href="{{ route('units.create') }}" class="btn btn-primary">Add New Unit</a>
		</div>
	</div>
	<hr>
</div>

	{{-- start accordion --}}
@foreach($equipments as $equipment)
<div class="accordion mb-1" id="equipments-group">
	<div class="container">
		<div class="card bg-dark">
			<div class="card-header" id="equipmentsHead">
				<div class="container">
					<div class="row">
						<div class="col col-12 col-md-8 mx-auto">
							<h3 class="text-left text-light">{{ $equipment->name }}</h3>
						</div>
						<div class="col col-12 col-md-4">
							<span class="btn btn-primary">{{ $equipment->current_available + $equipment->current_out }}</span>
							<span class="btn btn-danger">{{ $equipment->current_out }}</span>
							<span class="btn btn-warning">{{ $equipment->current_available }}</span>
							<button class="btn btn-light fas fa-arrow-alt-circle-down" type="button" data-toggle="collapse" data-target="#transaction-{{ $equipment->id }}" aria-expanded="true" aria-controls="collapseOne">
								VIEW LIST
							</button>
						</div>
					</div>
				</div>
			</div>
			{{-- end of equipment group head --}}


			<div id="transaction-{{ $equipment->id }}" class="collapse bg-light" aria-labelledby="equipmentsHead" data-parent="#equipments-group">
				<div class="card-body p-0">
					{{-- start of units cards --}}
					{{-- table starts here --}}
					<div class="table-responsive">
						<table class="table table-bordered text-center">
							<thead class="thead-default">
								<th scope="col col-md-1">No</th>
								<th scope="col col-md-2">Name</th>
								<th scope="col col-md-2">Year Model</th>
								<th scope="col col-md-2">Rent Price</th>
								<th scope="col col-md-2">Status</th>
								<th scope="col col-md-3">Action</th>
							</thead>
							<tbody>
								@foreach($equipment->units as $unit)

								{{-- Start of ROW for Equipments --}}
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td><strong>{{ $unit->name }}</strong></td>
									<td>{{ $unit->year_model }}</td>
									<td>&#8369; {{ number_format($unit->rent_price,2)  }}</td>
									<td>{{ strtoupper($unit->status->name) }}</td>
									<td>
										{{-- View Equipment Details --}}
										<a href="{{ route('units.show',[$unit->id]) }}" class="btn btn-light d-inline">
											<span class="fas fa-eye pr-2 pl-2"></span>
										</a>
										{{-- Edit Equipment Details --}}
										<a href="{{ route('units.edit',[$unit->id]) }}" class="btn btn-light d-inline">
											<span class="fas fa-edit pr-2 pl-2"></span>
											
										</a>
										{{-- Delete Equipment Details --}}
										<form action="{{ route('units.destroy',[$unit->id]) }}" method="POST" class="d-inline">
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-light"><span class="fas fa-trash-alt"></span></button>
										</form>
										
										
									</td>
									
								</tr>
								{{-- End of ROW for Equipments --}}
								@endforeach
							</tbody>
						</table>
					</div>
					{{-- table ends here --}}




					{{-- end of units cards --}}
				</div>
			</div>
		</div>
	</div>
</div>
{{-- end of accordion --}}
@endforeach













@endsection