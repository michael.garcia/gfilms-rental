{{-- Units -> Show / Display Individual --}}
{{-- show.blade.php --}}

@extends('layouts.app')
@section('content')


<div class="container">
	<h1 class="text-center">{{ $unit->name }}</h1>
	<hr>
	<div class="row">
		<div class="col col-12 col-md-8 mx-auto">
			<!-- Card Dark -->
			<div class="card">

			  <!-- Card image -->
			  <div class="view overlay">
			    <img class="card-img-top" src="https://via.placeholder.com/708x472" alt="Unit Image">
			    <a>
			      <div class="mask rgba-white-slight"></div>
			    </a>
			  </div>

			

			  <!-- Card content -->
			  <div class="card-body elegant-color white-text rounded-bottom">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-7 mx-auto">
								<h2>{{ $unit->name }}</h2>
								<p class="d-sm-block d-md-inline"><small>Manufactured By: </small><strong class="font-italic">{{ $unit->manufacturer }}</strong></p>
								<p class="pl-md-5 d-sm-block d-md-inline "><small>Year Model: </small><strong class="font-italic">{{ $unit->year_model }}</strong></p>
							</div>
							<div class="col-12 col-md-5 mx-auto">
								{{-- Edit Equipment Details --}}
								<a href="{{ route('units.edit',[$unit->id]) }}" class="btn btn-dark d-inline">Edit <span class="fas fa-edit pr-2 pl-2"></span></a>
								
								{{-- Delete Equipment Details --}}
								<form action="{{ route('units.destroy',[$unit->id]) }}" method="POST" class="d-inline">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-dark pt-1">Delete <span class="fas fa-trash-alt pr-2 pl-2"></span></button>
								</form>
							</div>
						</div>
					</div>
					
				{{-- End Title --}}

				<hr class="hr-light">
				
				{{-- Status --}}
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-7 mx-auto">
								<h2>Rental Price</h2>
							</div>
							<div class="col-12 col-md-5 mx-auto">
								<h2>&#8369; {{ number_format($unit->rent_price,2)  }}</h2>
							</div>
						</div>
					</div>

			    <hr class="hr-light">

			    <!-- Description -->
			    <p class="card-text white-text mb-4">{{ $unit->description }}</p>
				
				
			  </div>
			{{-- End Card Body --}}

			{{-- Start Card Footer --}}

				<div class="card-footer text-muted">
					<div class="text-center">
							<p class="d-sm-block d-md-inline">
								<small>Equipment Group:
									<strong class="font-italic">{{ $unit->equipment->name }}</strong> 
								</small>
								
							</p>
						  	<p class="pl-md-5 pr-md-5 d-sm-block d-md-inline">
						  		<small>Created Last: 
						  			<strong class="font-italic">
						  				{{ date('M j, Y  g:i A', strtotime($unit->created_at)) }}
						  			</strong>
						  		</small>
						  		
						  	</p>
						  	<p class="d-sm-block d-md-inline ">
						  		<small>Updated Last: 
									<strong class="font-italic">
										{{ date('M j, Y  g:i A', strtotime($unit->updated_at)) }}
									</strong>
						  		</small>
						  		
						  	</p>
					</div>
				</div>

			{{-- End Card Footer --}}
			
			</div>
			<!-- End Card  -->
		</div>


		<div class="col col-12 col-md-4 mx-auto">
			<a href="{{ route('units.index') }}"><h2 class="bg-primary text-light text-center rounded p-2 ">VIEW UNITS</h2></a>
			<h2 class="bg-dark text-light text-center rounded p-2 ">STATISTICS</h2>
			<div class="table-responsive rounded">
				<table class="table table-hover ">
					<thead>
						<th class="text-center">LEGEND</th>
						<th class="text-center">FIGURE</th>
					</thead>
					<tbody>

						<tr class="{{ $color }}">
							<td><strong>Status</strong></td>
							<td>{{ strtoupper($unit->status->name) }}</td>
						</tr>
						<tr>
							<td>Last Rented Date</td>
							<td>{Rented Date}</td>
						</tr>
						<tr>
							<td>Last Used By</td>
							<td>{Rented User}</td>
						</tr>
						<tr>
							<td>Total Times Rented</td>
							<td>{20}</td>
						</tr>
						<tr>
							<td>Gross Income</td>
							<td>&#8369; {{ number_format(150000,2)  }}</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>



	</div>
</div>

@endsection