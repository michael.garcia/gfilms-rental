{{-- Admin -> Equipments -> Edit --}}
{{-- edit.blade.php --}}

@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-md-5 mx-auto">
			<h3>Update Equipment</h3>
			<form action="{{ route('equipments.update',['equipment'=>$equipment->id]) }}" method="post">
				@csrf
				@method('PUT')
				<label for="name">Equipment Name: </label>
				<input type="text" name="name" value="{{ $equipment->name }}">
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
		<div class="col-12 col-md-7 mx-auto">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-9 mx-auto">
						<h3>Equipments List</h3>
					</div>
					<div class="col-12 col-md-3 mx-auto">
						<a href="{{ route('equipments.index') }}" class="btn btn-primary">View All</a>
					</div>
				</div>
			</div>
			{{-- table starts here --}}
			<div class="table-responsive">
				<table class="table table-bordered table-hover text-center">
					<thead class="thead-dark">
						<th scope="col">No.</th>
						<th scope="col">Name</th>
						<th scope="col">Total Units</th>
					</thead>
					<tbody>

						@foreach($equipments as $equipment)
						{{-- Start of Equipments Row --}}
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td><strong>{{ $equipment->name }}</strong></td>
							<td>{{ $equipment->current_available +  $equipment->current_out}}</td>
						</tr>
						{{-- End of Equipment Row --}}
						@endforeach
					</tbody>
				</table>
			</div>
			{{-- table ends here --}}
		</div>
	</div>
</div>


@endsection