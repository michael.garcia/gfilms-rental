{{-- Admin -> Equipments -> Index/Show All --}}
{{-- index.blade.php --}}

@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-9 mx-auto">
			<h3>Equipments Categories</h3>
		</div>
		<div class="col-12 col-md-3 mx-auto">
			<a href="{{ route('equipments.create') }}" class="btn btn-primary">Add New Equipment Group</a>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			{{-- table starts here --}}
			<div class="table-responsive">
				<table class="table table-bordered table-hover text-center">
					<thead class="thead-dark">
						<th scope="col col-md-1">No.</th>
						<th scope="col col-md-2">Name</th>
						<th scope="col col-md-2">Available</th>
						<th scope="col col-md-2">Unavailable</th>
						<th scope="col col-md-2">Total</th>
						<th scope="col col-md-3">Action</th>
					</thead>
					<tbody>
						@foreach($equipments as $equipment)

						{{-- Start of ROW for Equipments --}}
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td><strong>{{ $equipment->name }}</strong></td>
							<td>{{ $equipment->current_available }}</td>
							<td>{{ $equipment->current_out}}</td>
							<td>{{ $equipment->current_available +  $equipment->current_out}}</td>
							<td>
								{{-- View Equipment Details --}}
								<a href="{{ route('equipments.show',['equipment'=>$equipment->id]) }}" class="btn btn-light d-inline">
									<span class="fas fa-eye pr-2 pl-2"></span>
								</a>
								{{-- Edit Equipment Details --}}
								<a href="{{ route('equipments.edit',['equipment'=>$equipment->id]) }}" class="btn btn-light d-inline">
									<span class="fas fa-edit pr-2 pl-2"></span>
									
								</a>
								<form action="{{ route("equipments.destroy" , ['equipment'=>$equipment->id] ) }}" method="POST" class="d-inline">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-light"><span class="fas fa-trash-alt"></span></button>
								</form>
								
								
							</td>
							
						</tr>
						{{-- End of ROW for Equipments --}}
						@endforeach
					</tbody>
				</table>
			</div>
			{{-- table ends here --}}
		</div>
	</div>
</div>


@endsection