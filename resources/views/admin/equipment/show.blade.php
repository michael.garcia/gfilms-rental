{{-- Admin -> Equipments -> Show Individual --}}
{{-- show.blade.php --}}

@extends('layouts.app')
@section('content')


<div class="container">
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			<h3>Equipment Group Details</h3>
		</div>
		<div class="col-12 col-md-2 mx-auto">
			<a href="{{ route('equipments.index') }}" class="btn btn-primary">View All</a>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			{{-- table starts here --}}
			<div class="table-responsive">
				<table class="table table-bordered table-hover text-center">
					<thead class="thead-dark">
						<th scope="col">Name</th>
						<th scope="col">Available</th>
						<th scope="col">Unavailable</th>
						<th scope="col">Total</th>
					</thead>
					<tbody>
						{{-- Start of ROW for Equipments --}}
						<tr>
							<td><strong>{{ $equipment->name }}</strong></td>
							<td>{{ $equipment->current_available }}</td>
							<td>{{ $equipment->current_out}}</td>
							<td>{{ $equipment->current_available +  $equipment->current_out}}</td>							
						</tr>
						{{-- End of ROW for Equipments --}}
					</tbody>
				</table>
			</div>
			{{-- table ends here --}}
		</div>
	</div>
</div>

{{-- <div class="container">
	<div class="row">
		<div class="col-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead class="thead-dark tex">
						<th scope="col">Unit Name</th>
						<th scope="col">Maker</th>
						<th scope="col">Availability</th>
						<th scope="col">Rentable Price</th>
					</thead> --}}
					{{-- <tbody> --}}
						{{-- start of row for units --}}
						{{-- <tr>
							<td>"UNIT NAME"</td>
							<td>"UNIT MAKER"</td>
							<td>"AVAILABILITY"</td>
							<td>"RENTABLE PRICE"</td>
						</tr> --}}
						{{-- end of row for units --}}
					{{-- </tbody>
				</table>
			</div>
		</div>
	</div>
</div> --}}



@endsection
