<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rentable_Status extends Model
{
    protected $table = 'rentable_statuses';

    protected $guarded = [];
    
    public function units(){
            	return $this->belongsTo('App\Units', 'rentable_status_id');
    }
}
