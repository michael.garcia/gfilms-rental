<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	protected $guarded = [];
    
    public function equipment(){
    	return $this->belongsTo('App\Equipment','equipment_id');
    }

    public function status(){
    	return $this->belongsTo('App\Rentable_Status','rentable_status_id');
    }
}
