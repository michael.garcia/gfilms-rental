<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request_Status extends Model
{
    protected $table = 'request_statuses';

    protected $guarded = [];

    public function rentrequests(){
            	return $this->hasMany('App\Rentrequest','request_status_id');
            }

    
}
