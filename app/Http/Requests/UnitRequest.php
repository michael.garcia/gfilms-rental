<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'manufacturer' =>'string|required',
            'year_model'  =>'numeric|required',
            'description'  =>'string|required',
            'rent_price'  =>'numeric|required',
            'photo' =>'required|image|max:5000',
            'equipment_id' =>'numeric|required',
            'rentable_status_id' =>'numeric|required'
        ];
    }
}
