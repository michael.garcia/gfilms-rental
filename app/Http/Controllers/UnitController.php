<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Equipment;
use App\Rentable_Status;
// use App\Request\UnitRequest;
use Illuminate\Http\Request;

class UnitController extends Controller
{

    // public function addAll($equipments,$statuses,$units){
    //     $equipments = Equipment::all();
    //     $statuses = Rentable_Status::all();
    //     $units = Unit::all();

       

    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
  
        $equipments = Equipment::all();
        $statuses = Rentable_Status::all();


        return view('admin.units.index', compact('equipments'))
                 ->with('status',$statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('create',$unit);
        $equipments = Equipment::all();
        $units = Unit::all();
        $statuses = Rentable_Status::all();

        return view('admin.units.create', compact('equipments'))
                 ->with('status',$statuses)
                 ->with('unit',$units);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Unit $unit, Equipment $equipment)
    {
        
        
        $request->validate([
                    'name' => 'string|required',
                    'manufacturer' =>'string|required',
                    'year_model'  =>'numeric|required',
                    'description'  =>'string|required',
                    'rent_price'  =>'numeric|required',
                    'photo' =>'image|max:5000',
                    'equipment_id' =>'numeric|required'
                ]);




        $equipment_id = $request->input('equipment_id');
        $equipment = Equipment::find($equipment_id);

      
        // Save and Update Database
        $units = Unit::create($request->all());
        $equipment->current_available += 1;
        $equipment->save();

       


        if($units) {
            return redirect('units')->with('success', 'A unit was succesfully created!');
        }
        return redirect('units')->with('error', 'Unit not created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        $units = Unit::all();
        $color = 'table-warning';

        // color switching condition
        // dd($unit->status->id);
        if($unit->status->id !== 1)
        {
            $color = 'table-danger'; 
        }



        // dd($unavailable);
        return view('admin.units.show',compact('unit'))
            ->with('units',$units)
            ->with('color',$color);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipment $equipment, Unit $unit)
    {
        $units = Unit::all();
        $equipments = Equipment::all();
        $statuses = Rentable_Status::all();

        $equipment_option = $unit->equipment->id;
        $status_option = $unit->status->id;

        $color = 'table-warning';

        if($unit->status->id !== 1)
        {
            $color = 'table-danger'; 
        }

        return view('admin.units.edit')
               ->with('equipment',$equipment)
               ->with('equipments',$equipments)
               ->with('statuses',$statuses)
               ->with('units',$units)
               ->with('unit',$unit)
               ->with('equipment_option',$equipment_option)
               ->with('status_option',$status_option)
               ->with('color',$color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        

        // $equipment = Equipment::find($unit->equipment->id);
      
        $available = $unit->equipment->current_available;
        $items_out = $unit->equipment->current_out;
        $dbstatus = $unit->rentable_status_id;
        // echo "Old Available: ".$available."   | Old Out: ".$items_out."</br>";

        $request->validate([
                  'name' => 'string|required',
                  'manufacturer' =>'string|required',
                  'year_model'  =>'numeric|required',
                  'description'  =>'string|required',
                  'rent_price'  =>'numeric|required',
                  'photo' =>'image|max:5000',
                  'equipment_id' =>'numeric|required',
                  'rentable_status_id' =>'numeric|required'
                ]);


        $unit->name = $request->input('name');
        $unit->manufacturer = $request->input('manufacturer');
        $unit->year_model = $request->input('year_model');
        $unit->description = $request->input('description');
        $unit->rent_price = $request->input('rent_price');
        $unit->equipment_id = $request->input('equipment_id');
        $unit->rentable_status_id = $request->input('rentable_status_id');

        if($dbstatus == 1 && $unit->rentable_status_id !== 1)
        {
            $available -=1; 
            $items_out +=1;
        }
        elseif ($dbstatus !== 1 && $unit->rentable_status_id == 1) {
            $items_out -=1;
            $available +=1; 
        }
        else
        {
            $available = $unit->equipment->current_available;
            $items_out = $unit->equipment->current_out;
        }

        // echo "New Available: ".$available."   | New Out: ".$items_out."</br>";

        // dd($unit->equipment);

        $unit->equipment->current_available = $available;
        $unit->equipment->current_out = $items_out;
        $unit->equipment->save();



        $unit->save();
        $request->session()->flash('status','Update Successful');
       
       if($request) {
           return redirect('units')->with('success', 'A unit was succesfully created!');
       }
       return redirect('units')->with('error', 'Unit not created!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit, Request $request)
    {
        
        // declarations
        $available = $unit->equipment->current_available;
        $items_out = $unit->equipment->current_out;
        $dbstatus = $unit->rentable_status_id;

        // dd($dbstatus);
        // condition to check current stocks
        if($dbstatus == 1)
        {
            $available -=1; 
            // $items_out +=1;
        }
        elseif ($dbstatus !== 1) {
            $items_out -=1;
            // $available +=1; 
        }
        else
        {
            $available = $unit->equipment->current_available;
            $items_out = $unit->equipment->current_out;
        }

        // update stocks
        $unit->equipment->current_available = $available;
        $unit->equipment->current_out = $items_out;
        $unit->equipment->save();


        $unit->delete();
        return redirect('units')->with('success', 'A unit was succesfully deleted!');
    }
}
