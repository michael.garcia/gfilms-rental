<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    /**
             * Display a listing of the resource.
             *
             * @return \Illuminate\Http\Response
             */
            public function index()
            {
                $equipments = Equipment::all();
                return view('admin.equipment.index',compact('equipments'));
            }

            /**
             * Show the form for creating a new resource.
             *
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
                $equipments = Equipment::all();
                return view('admin.equipment.create',compact('equipments'));
            }

            /**
             * Store a newly created resource in storage.
             *
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function store(Request $request, Equipment $equipment)
            {
                
                $request->validate([
                    'name' => 'string|required|max:50|unique:equipments,name'
                ]);

                $equipments = Equipment::create($request->all());
                
                if ($equipments) {
                    return redirect('equipments')->with('success', 'Equipment succesfully created!');
                }
                return redirect('equipments')->with('error', 'Equipment not created!');


            }

            /**
             * Display the specified resource.
             *
             * @param  \App\Equipments  $equipments
             * @return \Illuminate\Http\Response
             */
            public function show(Equipment $equipment)
            {
                
                $equipments = Equipment::all();
                return view('admin.equipment.show')
                        ->with('equipment',$equipment)
                        ->with('equipments',$equipments);

            }

            /**
             * Show the form for editing the specified resource.
             *
             * @param  \App\Equipments  $equipments
             * @return \Illuminate\Http\Response
             */
            public function edit(Equipment $equipment)
            {
             
                $equipments = Equipment::all();
                return view('admin.equipment.edit')
                        ->with('equipment',$equipment)
                        ->with('equipments',$equipments);
            }

            /**
             * Update the specified resource in storage.
             *
             * @param  \Illuminate\Http\Request  $request
             * @param  \App\Equipments  $equipments
             * @return \Illuminate\Http\Response
             */
            public function update(Request $request, Equipment $equipment)
            {
               
                
                $request->validate([
                    'name' => 'string|required|max:50|unique:equipments,name'
                ]);

                $equipment->name = $request->input('name');
                $equipment->save();

                $equipments = Equipment::all();
                return view('admin.equipment.index',compact('equipments'));




            }

            /**
             * Remove the specified resource from storage.
             *
             * @param  \App\Equipments  $equipments
             * @return \Illuminate\Http\Response
             */
            public function destroy(Equipment $equipment)
            {
                $equipment->delete();
                $equipments = Equipment::all();
                return view('admin.equipment.index',compact('equipments'));          }
}
