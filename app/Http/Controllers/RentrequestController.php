<?php

namespace App\Http\Controllers;

use App\Rentrequest;
use Illuminate\Http\Request;

class RentrequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rentrequest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('rentrequest.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rentrequest  $rentrequest
     * @return \Illuminate\Http\Response
     */
    public function show(Rentrequest $rentrequest)
    {
        return view('rentrequest.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rentrequest  $rentrequest
     * @return \Illuminate\Http\Response
     */
    public function edit(Rentrequest $rentrequest)
    {
        return view('rentrequest.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rentrequest  $rentrequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rentrequest $rentrequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rentrequest  $rentrequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rentrequest $rentrequest)
    {
        //
    }
}
