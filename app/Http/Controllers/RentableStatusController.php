<?php

namespace App\Http\Controllers;

use App\Rentable_Status;
use Illuminate\Http\Request;

class RentableStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rentable_Status  $rentable_Status
     * @return \Illuminate\Http\Response
     */
    public function show(Rentable_Status $rentable_Status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rentable_Status  $rentable_Status
     * @return \Illuminate\Http\Response
     */
    public function edit(Rentable_Status $rentable_Status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rentable_Status  $rentable_Status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rentable_Status $rentable_Status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rentable_Status  $rentable_Status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rentable_Status $rentable_Status)
    {
        //
    }
}
