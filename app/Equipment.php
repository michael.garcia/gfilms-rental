<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
	protected $table = 'equipments';

	protected $guarded = [];

    public function units(){
            	return $this->hasMany('App\Unit');
            }
}
