<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rentrequest extends Model
{
    protected $table = 'rentrequests';

    protected $guarded = [];

    public function request_status(){
    	return $this->belongsTo('App\Request_Status','request_status_id');
    }


    public function users(){
    	return $this->belongsTo('App\User','user_id');
    }




}
